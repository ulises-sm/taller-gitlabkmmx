/*
 * 
 * Clase Ejemplo
 * 
 */
package tallergitlab;

/**
 *
 * @author ulises
 */
public class Matematicas {
    
    /**
     * 
     * @param a recibe un numero entero
     * @param b recibe un numero entero
     * @return returna la suma de a + b
     */
    public static Integer suma(Integer a, Integer b){
        try{
        return a + b;
        }catch(Exception ex){
            System.err.println("Error");
            return null;
        }
    }
    
}
