/*
 * 
 * Proyecto para Taller GitLab
 * 
 */
package tallergitlab;

/**
 *
 * @author ulises
 */
public class TallerGitLab {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Integer resultado = Matematicas.suma(1, 2);
        System.out.println("Suma: "+ resultado);
    }
    
}
